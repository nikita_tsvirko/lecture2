#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include <stdio.h>
#include <string>
#include "LongNum.h"

using namespace std;

LongNum::LongNum()
{
	fill(number, number + 2000, 0);
}

LongNum::LongNum(string num)
{
	length = 0;
	for (int i = num.length() - 1; i >= 0; i--) {
		number[length] = (int)num[i] - 48;
		length++;
	}
}

int LongNum::GetLength()
{
	return length;
}

LongNum LongNum::toPow(LongNum num, int pow)
{
	if (pow == 0)
		return LongNum("1");
	if (pow == 1)
		return num;
	if (pow % 2 == 1)
		return toPow(num, pow - 1)*num;
	LongNum newNum = toPow(num, pow / 2);
	return newNum*newNum;
}

void LongNum::Scan()
{
	string numInStr;
	cout << "������� ������� �����\n";
	getline(cin, numInStr, '\n');
	length = 0;
	for (int i = numInStr.length() - 1; i >= 0; i--) {
		number[length] = (int)numInStr[i] - 48;
		length++;
	}
	cout << endl;
}

void LongNum::Print()
{
	for (int i = length - 1; i >= 0; i--)
		cout << number[i];
	cout << endl;
}

LongNum operator*(LongNum l, LongNum r)
{
	LongNum res;
	res.length = l.length + r.length + 1;
	for (int i = 0; i < l.length; i++)
		for (int j = 0; j < r.length; j++)
			res.number[i + j] += l.number[i] * r.number[j];
	for (int i = 0; i < res.length; i++) {
		res.number[i + 1] += res.number[i] / 10;
		res.number[i] %= 10;
	}
	while (res.number[res.length] == 0)
		res.length--;
	res.length++;
	return res;
}
