#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include "LongNum.h"

using namespace std;

int main()
{
	SetConsoleOutputCP(1251);
	LongNum longNum;
	int pow;

	longNum.Scan();
	cout<<"Введите степень\n";
	cin >> pow;
	LongNum lonNumInPow = longNum.toPow(longNum, pow);
	lonNumInPow.Print();
	system("pause");
	return 0;
}