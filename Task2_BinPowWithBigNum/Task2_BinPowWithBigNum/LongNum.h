#pragma once
#include <iostream>
#include <string>

using namespace std;

class LongNum {
	short number[2000];
	int length;
public:
	LongNum();
	LongNum(string num);
	int GetLength();
	LongNum toPow(LongNum num, int pow);
	void Scan();
	void Print();
	friend LongNum operator*(LongNum l, LongNum r);
};